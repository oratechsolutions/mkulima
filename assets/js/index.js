/* eslint-disable */
$(document).ready(function() {
    // @ts-check

    var XMLHttpFactories = [
        {
            name: 'Microsoft.XMLHTTP',
            fnc: function() {
                return new ActiveXObject('Microsoft.XMLHTTP');
            },
        },
        {
            name: 'Msxml2.XMLHTTP',
            fnc: function() {
                return new ActiveXObject('Msxml2.XMLHTTP');
            },
        },
        {
            name: 'Msxml3.XMLHTTP',
            fnc: function() {
                return new ActiveXObject('Msxml3.XMLHTTP');
            },
        },
        {
            name: 'XMLHttpRequest',
            fnc: function() {
                return new XMLHttpRequest();
            },
        },
    ];

    /**
     * creates ajax request, compatible with IE
     * @returns {XMLHttpRequest}
     */
    function createXMLHTTPObject() {
        var xmlhttp;
        for (var i = 0; i < XMLHttpFactories.length; i++) {
            try {
                xmlhttp = XMLHttpFactories[i].fnc();
            } catch (e) {
                continue;
            }
            break;
        }
        return xmlhttp;
    }

    /**
     *
     * Top level ajax utility function
     * @param {string} url server url to fetch or send data
     * @param {string|Document|ArrayBuffer|FormData|ReadableStream|Function} data data to send on POST requests
     * @param {Function=} callback callback function to access data from server
     */
    function request(url, data, callback) {
        var method = 'GET';
        if (typeof url == 'undefined') throw new Error('First argument must be passed!');

        if (typeof callback != 'undefined') {
            method = 'POST';
        }
        if (typeof data == 'function') {
            callback = data;
        }

        var xhr = createXMLHTTPObject();
        xhr.open(method, url, true);
        xhr.withCredentials = true;
        xhr.setRequestHeader('Cache-Control', 'no-cache');
        if (method == 'POST') {
            xhr.setRequestHeader('Content-Type', 'application/json');
            data = JSON.stringify(data);
        }

        // @ts-ignore
        method == 'GET' ? xhr.send(null) : xhr.send(data);

        xhr.addEventListener('loadend', function() {
            var self = this;
            this.status == 200 && this.readyState == 4
                ? (function() {
                      return callback(self.responseText);
                  })()
                : (function() {
                      return callback({
                          error: self.response,
                      });
                  })();
        });
    }

    var panelOne = $('.form-panel.two').height(),
        panelTwo = $('.form-panel.two')[0].scrollHeight;

    $('.form-panel.two')
        .not('.form-panel.two.active')
        .on('click', function(e) {
            e.preventDefault();

            $('.form-toggle').addClass('visible');
            $('.form-panel.one').addClass('hidden');
            $('.form-panel.two').addClass('active');
            $('.form').animate(
                {
                    height: panelTwo,
                },
                200
            );
        });

    $('.form-toggle').on('click', function(e) {
        e.preventDefault();
        $(this).removeClass('visible');
        $('.form-panel.one').removeClass('hidden');
        $('.form-panel.two').removeClass('active');
        $('.form').animate(
            {
                height: panelOne,
            },
            200
        );
    });

    function extractFormData(form) {
        if (typeof form == 'undefined') throw new Error('Requires a form to iterate');
        else {
            var data = {};
            // reset object values first before iterating form
            Object.getOwnPropertyNames(data).forEach(key => {
                delete data[key];
            });
            for (let element of Array.from(form.elements)) {
                ['text', 'number', 'url', 'textarea', 'password', 'email'].forEach(type => {
                    if (element['type'].indexOf(type) != -1) {
                        data[element['name']] = element['value'].trim();
                    }
                });
                if (element['type'] == 'file') {
                    data[element['name']] = element['files'][0];
                }
                if (element['type'].indexOf('select') != -1) {
                    if (element.getAttribute('multiple') != undefined) {
                        try {
                            let multiValues = [],
                                options = Array.from(element.querySelectorAll('option'));
                            for (let option of options) {
                                if (option.selected) multiValues.push(option.value);
                            }
                            data[element['name']] = multiValues;
                        } catch (err) {}
                    } else {
                        data[element['name']] = element['value'];
                    }
                }
                // @ts-ignore
                if (element['type'] == 'checkbox' && element.checked) data[element['name']] = element['value'];
                // @ts-ignore
                if (element['type'] == 'radio' && element.checked) data[element['name']] = element['value'];
            }
            // delete empty keys
            Object.keys(data).forEach(function(key) {
                key.length < 1 && delete data[key];
            });
            return data;
        }
    }

    var loginform = document.getElementById('loginform');

    loginform.addEventListener('submit', function(event) {
        event.preventDefault();
        var logindata = extractFormData(loginform);
        request('/api/auth/signin', logindata, function(res) {
            console.log(res);
        });
    });
});
