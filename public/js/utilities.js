var XMLHttpFactories = [
    {
        name: 'Microsoft.XMLHTTP',
        fnc: function() {
            return new ActiveXObject('Microsoft.XMLHTTP');
        },
    },
    {
        name: 'Msxml2.XMLHTTP',
        fnc: function() {
            return new ActiveXObject('Msxml2.XMLHTTP');
        },
    },
    {
        name: 'Msxml3.XMLHTTP',
        fnc: function() {
            return new ActiveXObject('Msxml3.XMLHTTP');
        },
    },
    {
        name: 'XMLHttpRequest',
        fnc: function() {
            return new XMLHttpRequest();
        },
    },
];

/**
 * creates ajax request, compatible with IE
 * @returns {XMLHttpRequest}
 */
function createXMLHTTPObject() {
    var xmlhttp;
    for (var i = 0; i < XMLHttpFactories.length; i++) {
        try {
            xmlhttp = XMLHttpFactories[i].fnc();
        } catch (e) {
            continue;
        }
        break;
    }
    return xmlhttp;
}

/**
 * Top level ajax utility function, to fetch and send data to the server.
 *
 * @param {string} url server url to fetch or send data.
 * @param {string|Document|ArrayBuffer|FormData|ReadableStream=} data data to send on POST requests.
 * @param {boolean=} form true if data to send to server contains file(s) to upload.
 * @param {Function=} callback callback function with data returned by the server.
 */
function request(url, data, form, callback) {
    var method = 'GET';
    if (typeof url === 'undefined') {
        throw new Error('First argument must be passed!');
    }

    if (typeof form === 'function') {
        method = 'POST';
        callback = form;
    }

    if (typeof data === 'function') {
        form = data;
        callback = form;
    }

    if (typeof form === 'boolean') {
        method = 'POST';
        var formdata = new FormData();

        var keys = Object.keys(data);
        keys.forEach(function(key) {
            formdata.append(key, data[key]);
        });

        data = formdata;
    }

    var xhr = createXMLHTTPObject();
    xhr.open(method, url, true);
    xhr.withCredentials = true;
    xhr.setRequestHeader('Cache-Control', 'no-cache');
    if (method === 'POST' && typeof form === 'function') {
        xhr.setRequestHeader('Content-Type', 'application/json');
        data = JSON.stringify(data);
    }

    method === 'GET' ? xhr.send(null) : xhr.send(data);

    xhr.addEventListener('loadend', function() {
        var self = this;
        if (self.status === 200 && self.readyState === 4) {
            return callback(self.responseText);
        } else {
            return callback({ error: self.response });
        }
    });
}

/**
 *
 * Extracts data from form and returns an iterable
 *
 * @param {HTMLFormElement} form HTMLFormElement
 * @returns {{}}
 */
function extractFormData(form) {
    if (typeof form === 'undefined') {
        throw new Error('Requires a form to iterate');
    } else {
        var data = {};
        // reset object values first before iterating form
        Object.getOwnPropertyNames(data).forEach(function(key) {
            delete data[key];
        });
        for (var element of Array.from(form.elements)) {
            ['text', 'number', 'url', 'textarea', 'password', 'email'].forEach(function(type) {
                if (element.type.indexOf(type) !== -1) {
                    data[element.name.trim()] = element.value.trim();
                }
            });
            if (element.type === 'file') {
                data[element.name.trim()] = element.files[0];
            }
            if (element.type.indexOf('select') !== -1) {
                if (element.getAttribute('multiple') !== undefined) {
                    try {
                        var multiValues = [];
                        var options = Array.from(element.querySelectorAll('option'));
                        for (var option of options) {
                            if (option.selected) {
                                multiValues.push(option.value.trim());
                            }
                        }
                        data[element.name.trim()] = multiValues;
                        // tslint:disable-next-line:no-empty
                    } catch (err) {}
                } else {
                    data[element.name.trim()] = element.value.trim();
                }
            }
            // @ts-ignore
            if (element.type === 'checkbox' && element.checked) {
                data[element.name.trim()] = element.value.trim();
            }
            // @ts-ignore
            if (element.type === 'radio' && element.checked) {
                data[element.name.trim()] = element.value.trim();
            }
        }
        // delete entries with empty keys
        Object.keys(data).forEach(function(key) {
            return key.length < 1 && delete data[key];
        });
        return data;
    }
}

/**
 * checks if cookie exists and returns all cookies, or cookie value if cookie name is passed
 * @param {string} cookieString
 * @param {string=} cookieName
 */
function extractCookies(cookieName) {
    // @ts-ignore
    let c = decodeURIComponent(document.cookie);
    let d;
    let e = {};
    if (c.length < 1) {
        return false;
    }
    if (c.indexOf(';') !== -1) {
        d = c.split(';');
    } else {
        d = c;
    }
    if (typeof d === 'string') {
        e[d.split('=')[0].trim()] = d.split('=')[1].trim();
    } else {
        d.map(function(p) {
            return (e[p.split('=')[0].trim()] = p.split('=')[1].trim());
        });
    }

    if (typeof cookieName !== 'undefined') {
        return e[cookieName];
    }
    return e;
}

$(document).ready(function() {
    $('#farmersTable').dataTable();

    var currentuserkey = {};
    var addfarmerform = document.getElementById('addfarmerform');
    if (addfarmerform) {
        var btn = document.getElementById('registerFarmer');
        addfarmerform.addEventListener('submit', function(e) {
            btn.disabled = true;
            btn.innerHTML = '<span>Registering ... <i class="mdi mdi-loading mdi-spin"></i></span>';
            e.preventDefault();
            var data = extractFormData(addfarmerform);
            request('/data/farmers', data, function(res) {
                res = JSON.parse(res);
                if (res.error && res.error === 'farmer-exists') {
                    btn.innerHTML = '<span>Farmer exists <span class="mdi mdi-alert"></span></span>';
                    setTimeout(function() {
                        addfarmerform.reset();
                        btn.disabled = false;
                        btn.innerHTML = '<span>Register farmer <span class="mdi mdi-account-plus"></span>';
                    }, 3000);
                }
                if (res.message && res.message === 'success') {
                    btn.innerHTML = '<span>Successfully registered!<span class="mdi mdi-check-circle"></span></span>';
                    setTimeout(function() {
                        addfarmerform.reset();
                        btn.disabled = false;
                        btn.innerHTML = '<span>Register farmer <span class="mdi mdi-account-plus"></span>';
                    }, 3000);
                }
                if (res.message && res.message === 'failed') {
                    btn.innerHTML = '<span>Unknown server error occured!<span class="mdi mdi-alert"></span></span>';
                    setTimeout(function() {
                        addfarmerform.reset();
                        btn.disabled = false;
                        btn.innerHTML = '<span>Register farmer <span class="mdi mdi-account-plus"></span></span>';
                    }, 3000);
                }
            });
        });
    }

    var chatbox = document.querySelector('.nice-chat');
    var indicator = document.getElementById('chatprogressindicator');
    /**
     * Retrieve consultations.
     *
     * @param {string=} farmer
     */
    function retrieveConsultations() {
        var cookies = extractCookies();
        var currentuser = '';
        for (var cookie in cookies) {
            if (cookie.includes('mkl')) {
                currentuser = cookies[cookie];
            }
        }

        var farmerId = currentuserkey.user;

        request('/data/consultation/' + farmerId, function(data) {
            data = JSON.parse(data);
            var chats = '';
            if (!Array.from(data).length) {
                chats = '<div style="margin: 120px 0px;">No previous consultations found.' + 'Send a message to start the chat.</div>';
            } else {
                Array.from(data).forEach(function(chatData) {
                    chats += `
                            <li class="${currentuser === chatData.receiver ? 'out' : 'in'}">
                                <div class="message">
                                     <span class="datetime">
                                        ${moment(chatData.createdat).format('MMMM Do YYYY, h:mm:ss a')}
                                     </span>
                                    <span class="body"> ${chatData.message} </span>
                                </div>
                            </li>
                        `;
                });
                markReadMessages(data, currentuser);
            }
            chatbox.innerHTML = chats;
            var chat = document.querySelector('.nice-chat');
            chat.scrollTo(0, chat.scrollHeight);
            indicator.classList.remove('d-flex');
            indicator.classList.add('d-none');
        });
    }

    /**
     * Mark messages as read on open view
     *
     * @param {string[]} chats
     * @param {string} currentuser
     */
    function markReadMessages(chats, currentuser) {
        var chatIds = chats
            .map(function(chat) {
                if (chat.receiver === currentuser && !chat.viewed) {
                    return chat._id;
                }
            })
            .filter(function(id) {
                return id !== undefined;
            });
        request('/data/mark-read-consultations', { chatIds, sender: chats[0].receiver, receiver: chats[0].sender }, function(res) {
            consultationCounter();
        });
    }

    var users = document.querySelectorAll('.consultations');

    var chatInterval = null;

    if (users) {
        users.forEach(function(user) {
            user.addEventListener('click', function() {
                indicator.classList.remove('d-none');
                indicator.classList.add('d-flex');
                var username = document.getElementById('username');
                var selectedchat = user.querySelector('.username');
                username.innerText = selectedchat.innerText;
                currentuserkey.user = user.getAttribute('data-user');
                if (chatInterval) {
                    clearInterval(chatInterval);
                }
                chatInterval = setInterval(function() {
                    retrieveConsultations();
                }, 1000);
                retrieveConsultations();
            });
        });
    }

    var chatForm = document.getElementById('chatForm');
    if (chatForm) {
        chatForm.addEventListener('submit', function(e) {
            e.preventDefault();

            var message = extractFormData(chatForm);

            var data = { ...message, receiver: currentuserkey.user };

            request('/data/consultation', data, function(res) {
                retrieveConsultations();
                chatForm.reset();
            });
        });
    }

    var updateOfficerProfile = document.getElementById('updateOfficerProfile');
    if (updateOfficerProfile) {
        updateOfficerProfile.addEventListener('submit', function(e) {
            e.preventDefault();
            var data = extractFormData(updateOfficerProfile);
            request('/data/officer/update', data, true, function(req, res) {
                // tslint:disable-next-line:no-console
                console.log(res);
            });
        });
    }

    var updateFarmerProfile = document.getElementById('updateFarmerProfile');
    if (updateFarmerProfile) {
        updateFarmerProfile.addEventListener('submit', function(e) {
            e.preventDefault();
            var data = extractFormData(updateFarmerProfile);
            request('/data/farmer/update', data, true, function(req, res) {
                // tslint:disable-next-line:no-console
                console.log(res);
            });
        });
    }

    function consultationCounter() {
        request('/data/consultations', function(data) {
            data = JSON.parse(data);
            $('.messageCounter').html(data.length);
            users.forEach(function(user) {
                var userId = user.getAttribute('data-user');
                var counterDisplay = user.querySelector('.mark-readed');

                var match = data.map(function(counter) {
                    return counter.sender;
                });
                if (match[0] === userId) {
                    counterDisplay.innerText = match.length;
                } else {
                    counterDisplay.innerText = 0;
                }
            });
        });
        request('/data/grouped-consultations', function(data) {
            console.log(data);
        });
    }

    setInterval(function() {
        consultationCounter();
    }, 2000);
});
