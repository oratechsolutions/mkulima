$(function() {
    var XMLHttpFactories = [
        {
            name: 'Microsoft.XMLHTTP',
            fnc: function() {
                return new ActiveXObject('Microsoft.XMLHTTP');
            },
        },
        {
            name: 'Msxml2.XMLHTTP',
            fnc: function() {
                return new ActiveXObject('Msxml2.XMLHTTP');
            },
        },
        {
            name: 'Msxml3.XMLHTTP',
            fnc: function() {
                return new ActiveXObject('Msxml3.XMLHTTP');
            },
        },
        {
            name: 'XMLHttpRequest',
            fnc: function() {
                return new XMLHttpRequest();
            },
        },
    ];

    /**
     * creates ajax request, compatible with IE
     * @returns {XMLHttpRequest}
     */
    function createXMLHTTPObject() {
        var xmlhttp;
        for (var i = 0; i < XMLHttpFactories.length; i++) {
            try {
                xmlhttp = XMLHttpFactories[i].fnc();
            } catch (e) {
                continue;
            }
            break;
        }
        return xmlhttp;
    }

    /**
     * Top level ajax utility function, to fetch and send data to the server.
     *
     * @param {string} url server url to fetch or send data.
     * @param {string|Document|ArrayBuffer|FormData|ReadableStream=} data data to send on POST requests.
     * @param {boolean=} form true if data to send to server contains file(s) to upload.
     * @param {Function=} callback callback function with data returned by the server.
     */
    function request(url, data, form, callback) {
        var method = 'GET';
        if (typeof url === 'undefined') {
            throw new Error('First argument must be passed!');
        }

        if (typeof form === 'function') {
            method = 'POST';
            callback = form;
        }

        if (typeof data === 'function') {
            form = data;
            callback = form;
        }

        if (typeof form === 'boolean') {
            method = 'POST';
            var formdata = new FormData();
            var keys = Object.keys(data);
            keys.forEach(function(key) {
                formdata.append(key, data[key]);
            });
            data = formdata;
        }

        var xhr = createXMLHTTPObject();
        xhr.open(method, url, true);
        xhr.withCredentials = true;
        xhr.setRequestHeader('Cache-Control', 'no-cache');
        if (method === 'POST' && typeof form === 'function') {
            xhr.setRequestHeader('Content-Type', 'application/json');
            data = JSON.stringify(data);
        }

        method === 'GET' ? xhr.send(null) : xhr.send(data);

        xhr.addEventListener('loadend', function() {
            var self = this;
            if (self.status === 200 && self.readyState === 4) {
                return callback(self.responseText);
            } else {
                return callback({ error: self.response });
            }
        });
    }

    /**
     *
     * Extracts data from form and returns an iterable
     *
     * @param {HTMLFormElement} form HTMLFormElement
     * @returns {{}}
     */
    function extractFormData(form) {
        if (typeof form === 'undefined') {
            throw new Error('Requires a form to iterate');
        } else {
            var data = {};
            // reset object values first before iterating form
            Object.getOwnPropertyNames(data).forEach(key => {
                delete data[key];
            });
            for (var element of Array.from(form.elements)) {
                ['text', 'number', 'url', 'textarea', 'password', 'email'].forEach(type => {
                    if (element.type.indexOf(type) !== -1) {
                        data[element.name.trim()] = element.value.trim();
                    }
                });
                if (element.type === 'file') {
                    data[element.name.trim()] = element.files[0];
                }
                if (element.type.indexOf('select') !== -1) {
                    if (element.getAttribute('multiple') !== undefined) {
                        try {
                            var multiValues = [],
                                options = Array.from(element.querySelectorAll('option'));
                            for (var option of options) {
                                if (option.selected) {
                                    multiValues.push(option.value.trim());
                                }
                            }
                            data[element.name.trim()] = multiValues;
                        } catch (err) {}
                    } else {
                        data[element.name.trim()] = element.value.trim();
                    }
                }
                // @ts-ignore
                if (element.type === 'checkbox' && element.checked) {
                    data[element.name.trim()] = element.value.trim();
                }
                // @ts-ignore
                if (element.type === 'radio' && element.checked) {
                    data[element.name.trim()] = element.value.trim();
                }
            }
            // delete entries with empty keys
            Object.keys(data).forEach(key => key.length < 1 && delete data[key]);
            return data;
        }
    }

    /**
     * checks if cookie exists and returns all cookies, or cookie value if cookie name is passed
     * @param {string} cookieString
     * @param {string=} cookieName
     */
    function extractCookies(cookieName) {
        // @ts-ignore
        let c = decodeURIComponent(document.cookie),
            d,
            e = {};
        if (c.length < 1) {
            return false;
        }
        if (c.indexOf(';') !== -1) {
            d = c.split(';');
        } else {
            d = c;
        }
        if (typeof d === 'string') {
            e[d.split('=')[0].trim()] = d.split('=')[1].trim();
        } else {
            d.map(p => (e[p.split('=')[0].trim()] = p.split('=')[1].trim()));
        }

        if (typeof cookieName !== 'undefined') {
            return e[cookieName];
        }
        return e;
    }

    // company registration
    var registerCompanyForm = document.getElementById('registerCompanyForm');
    registerCompanyForm.addEventListener('submit', function(event) {
        event.preventDefault();
        var data = extractFormData(registerCompanyForm);
        request('/data/company-data', data, true, function(res) {
            console.log(res);
        });
    });
});
