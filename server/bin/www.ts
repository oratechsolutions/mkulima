#!/usr/bin/env node
import * as bodyParser from 'body-parser';
import * as cookieParser from 'cookie-parser';
import * as express from 'express';
import { createReadStream, existsSync, readFile } from 'fs';
import { createServer, Server } from 'http';
import * as mongoose from 'mongoose';
import * as path from 'path';
import * as serveFavicon from 'serve-favicon';
import { MONGO_LOCAL_URL, MONGO_PROD_URL } from '../configs/config';
import * as cors from 'cors';

/**
 * Server class, configs and route handler
 *
 * @export
 * @class Mkulima
 */
export class Mkulima {
    /**
     * Instance of express application object
     *
     * @private
     * @type {express.Application}
     * @memberof Mkulima
     */
    private app: express.Application;

    /**
     * Instance of http server object
     *
     * @private
     * @type {Server}
     * @memberof Mkulima
     */
    private server: Server;

    /**
     * Port number for application to listen to
     *
     * @private
     * @type {(number | string)}
     * @memberof Mkulima
     */
    private port: number | string;

    constructor() {
        this.port = process.env.PORT || 3000;
        this.app = express();
        this.server = createServer(this.app);
        this.configs();
        this.routes();
    }

    /**
     * Start server instance
     *
     * @memberof Mkulima
     */
    public async start() {
        const port = this.normalizePort(this.port);
        this.server.listen(port, () => {
            console.log(`Server process: ${process.pid} listening on port: ${port}`);
        });
    }

    /**
     * Top level server configs
     *
     * @private
     * @memberof Mkulima
     */
    private configs() {
        this.app.use(express.static(path.join(__dirname, '..', '..', 'public')));
        this.app.use(express.static(path.join(__dirname, '..', '..', 'uploads')));
        this.app.set('view engine', 'ejs');
        this.app.set('views', path.join(__dirname, '..', '..', 'views'));
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(bodyParser.json());
        this.app.use(cors({ origin: true, credentials: true, preflightContinue: true }));
        this.app.use(cookieParser());
        this.app.use(serveFavicon(path.join(__dirname, '..', '..', 'favicon.ico')));
        this.app.use((req, res, next) => {
            res.setHeader('X-Frame-Options', 'SAMEORIGIN'); // disable viewing in iframe
            next();
        });

        mongoose
            .connect(
                process.env.NODE_ENV === 'production' ? MONGO_PROD_URL : MONGO_LOCAL_URL,
                { useNewUrlParser: true }
            )
            .then(() => {
                console.log('Mongo connected successfully');
            })
            .catch(e => console.log(e));
    }

    /**
     * Server routes handler
     *
     * @privates
     * @memberof Mkulima
     */
    private routes() {
        this.app.use('/', require('../routes'));
        /*
         * handle production requests, both view, data and authentication requests.
         * route all traffic to index.html for client-side rendering and routing using Vue
         * api requests will be made from vue, an alternative would be to allow calls to API endpoint
         * before forwading to client side vue to allow calls from other client side applications like android and other
         * client applications.
         * to enable this, the /api path is initialized before handing over to vue.
         */

        // handler for static resources
        this.app.get(/.*/, (req, res) => {
            const rootPath = path.join(__dirname, '..', '..' + req.url),
                mimeType = Object.create({
                    '.js': 'text/javascript',
                    '.css': 'text/css',
                    '.html': 'text/html',
                    '.woff2': 'font/woff2',
                });
            existsSync(path.resolve(rootPath))
                ? (function() {
                      readFile(path.resolve(rootPath), (err, data) => {
                          err
                              ? (function() {
                                    res.writeHead(500, 'Internal server error', {
                                        'Content-Type': 'text/plain',
                                    });
                                    res.end();
                                })()
                              : (function() {
                                    if (path.extname(rootPath) === '.html') {
                                        createReadStream(rootPath).pipe(res);
                                    } else {
                                        res.writeHead(200, {
                                            'Content-Type': mimeType[path.extname(rootPath)],
                                        });
                                        createReadStream(rootPath).pipe(res);
                                    }
                                })();
                      });
                  })()
                : (function() {
                      res.writeHead(404, 'Ruquested file not found');
                      res.end();
                  })();
        });
    }

    /**
     * Normalize port to ensure a number is passed
     *
     * @private
     * @param {(string | number)} port
     * @returns
     * @memberof Mkulima
     */
    private normalizePort(port: string | number): number {
        if (typeof port !== 'string' && typeof port !== 'number') {
            throw new TypeError(`Argument of type ${typeof port} cannot be used as port!`);
        }
        return Number(port);
    }
}
