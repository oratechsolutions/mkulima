// controller for Officer officers
import { hashSync } from 'bcrypt';
import { randomBytes } from 'crypto';
import { Request, Response } from 'express';
import { Document, Types } from 'mongoose';
import { Controller } from '../abstracts/Controller';
import { Email } from '../libraries/Email';
import { extractCookie } from '../middlewares/Cookies';
import Officer from '../models/Officer';
import Emailcreator from '../utilities/Emailcreator';

/**
 * System admin controller. Manage interaction with Admin model, create, update, retrieve and remove system administrators.
 *
 * @export
 * @class Officers
 * @extends {Controller}
 */
export class Officers extends Controller {
    public constructor(req: Request, res: Response) {
        super(req, res);
    }

    public async findOneAndUpdate(): Promise<Response> {
        const cookie = extractCookie(this.request.headers.cookie, 'mkl-xt-ssid');
        const data = {
            name: this.body['firstname'] + ' ' + this.body['secondname'],
            email: this.body['email'],
            gender: this.body['gender'],
            phone: '+254 ' + this.body['phone'].slice(-9),
            location: this.body['location'],
            expertise: this.body['expertise'],
        };

        if (!this.body['avatar']) {
            data['avatar'] = {
                path: this.request.files[0]['filename'],
            };
        }
        if (this.body['password']) {
            data['password'] = hashSync(this.body['password'], 10);
        }

        const updated = await Officer.findOneAndUpdate(
            {
                _id: Types.ObjectId(cookie.toString()),
            },
            {
                $set: { ...data },
            },
            {
                new: true,
            }
        );

        return this.response.status(200).json({ message: 'updated' });
    }

    /**
     * Find and return all Officers data, including profile information
     *
     * @returns {Promise<Response>}
     * @memberof Officers
     */
    public async findAllEntries(internal?: boolean): Promise<Response | any> {
        const officers = await Officer.aggregate([
            {
                $match: {},
            },
            {
                $project: { name: 1, email: 1, avatar: 1, phone: 1 },
            },
            // {
            //     $group: { _id: { admin: '$admin' } },
            // },
        ]);

        return internal ? officers : this.response.status(200).json(officers);
    }

    /**
     * Create a new extension officer
     *
     * @returns {Promise<Response>}
     * @memberof Officers
     */
    public async addNewEntry(): Promise<Response> {
        const admin: Document[] = <Document[]>await this.findOneEntry(true);

        if (admin.length) {
            return this.response.status(200).json({ error: 'extension-officer-exists' });
        }

        const password = randomBytes(3)
            .toString('hex')
            .toUpperCase();

        const data = {
            vcode: randomBytes(6)
                .toString('hex')
                .toUpperCase(),
            name: this.body['name'],
            email: this.body['email'],
            password: hashSync(password, 10),
            admin: extractCookie(this.request.headers.cookie, 'mkl-a-ssid'),
        };

        const profile = new Officer({ _id: new Types.ObjectId(), ...data });

        const vemail = await new Emailcreator({
            template: 'confirmation',
        }).compile({ email: data.email, code: data.vcode, password: password });

        const emailstatus = await new Email({
            recipients: [data.email],
            message: vemail,
            subject: 'Verify your account',
            fromtext: 'Mkulima service platform.',
            tocustomname: data.name,
        }).send();

        const created = await profile.save();

        if (created['email'] == data.email) {
            // prompt to check their mail inboxes for verification code / link
            return this.response.status(200).json({ message: 'success' });
        }

        return this.response.status(200).json({ message: 'failed' });
    }

    /**
     * Find a Admin by their email address
     *
     * @param {boolean} [internal]
     * @returns {(Promise<Document[] | Response>)}
     * @memberof Officers
     */
    public async findOneEntry(internal?: boolean): Promise<Document[] | Response> {
        const criteria: string = internal ? this.body['email'] : extractCookie(this.request.headers.cookie, 'mkl-xt-ssid');

        const match: Document[] = await Officer.aggregate([{ $match: { email: criteria } }]);
        return internal ? match : this.response.status(200).json(match);
    }
}
