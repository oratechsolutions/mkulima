import { hashSync } from 'bcrypt';
import { randomBytes } from 'crypto';
import { Request, Response } from 'express';
import { Document, Types } from 'mongoose';
import { Controller } from '../abstracts/Controller';
import { Email } from '../libraries/Email';
import Sysadmin from '../models/Sysadmin';
import Emailcreator from '../utilities/Emailcreator';

/**
 * System admin controller. Manage interaction with sysadmin model, create, update, retrieve and remove system administrators.
 *
 * @export
 * @class Sysadmins
 * @extends {Controller}
 */
export class Sysadmins extends Controller {
    public constructor(req: Request, res: Response) {
        super(req, res);
    }

    public async findOneAndUpdate(): Promise<Response> {
        throw new Error('Method not implemented.');
    }

    /**
     * Find and return all Sysadmins data, including profile information
     *
     * @returns {Promise<Response>}
     * @memberof Sysadmins
     */
    public async findAllEntries(): Promise<Response> {
        const sysadmins = await Sysadmin.aggregate([
            {
                $project: { username: 1, email: 1, avatar: 1, roles: 1 },
            },
        ]);

        return this.response.status(200).json(sysadmins);
    }

    /**
     * Create a new Sysadmin
     *
     * @returns {Promise<Response>}
     * @memberof Sysadmins
     */
    public async addNewEntry(): Promise<Response> {
        const sysadmin: Document[] = <Document[]>await this.findOneEntry(true);

        if (sysadmin.length) {
            return this.response.status(200).json({ error: 'user-exists' });
        }

        const data = {
            vcode: randomBytes(6)
                .toString('hex')
                .toUpperCase(),
            name: this.body['name'],
            email: this.body['email'],
            password: hashSync(this.body['password'], 10),
        };

        const profile = new Sysadmin({
            _id: new Types.ObjectId(),
            ...data,
        });

        const vemail = await new Emailcreator({
            template: 'confirmation',
        }).compile({ email: data.email, code: data.vcode });

        const emailstatus = await new Email({
            recipients: [data.email],
            message: vemail,
            subject: 'Verify your account',
            fromtext: 'Mkulima service platform.',
            tocustomname: data.name,
        }).send();

        const created = await profile.save();

        if (created['email'] == data.email) {
            // prompt to check their mail inboxes for verification code / link
            return this.response.status(200).json({ message: 'success' });
        }

        return this.response.status(200).json({ message: 'failed' });
    }

    /**
     * Find a Sysadmin by their email address
     *
     * @param {boolean} [internal]
     * @returns {(Promise<Document[] | Response>)}
     * @memberof Sysadmins
     */
    public async findOneEntry(internal?: boolean): Promise<Document[] | Response> {
        const criteria: string = internal ? this.body['email'] : this.request.params['email'];

        const match: Document[] = await Sysadmin.aggregate([{ $match: { email: criteria } }]);
        return internal ? match : this.response.status(200).json(match);
    }
}
