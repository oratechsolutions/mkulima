import { Request, Response } from 'express';
import { Document, Types } from 'mongoose';
import { Controller } from '../abstracts/Controller';
import Company from '../models/Company';

/**
 * System admin controller. Manage interaction with sysadmin model, create, update, retrieve and remove system administrators.
 *
 * @export
 * @class Sysadmins
 * @extends {Controller}
 */
export class Companies extends Controller {
    public constructor(req: Request, res: Response) {
        super(req, res);
    }

    public async findOneAndUpdate(): Promise<Response> {
        throw new Error('Method not implemented.');
    }

    /**
     * Find and return all Sysadmins data, including profile information
     *
     * @returns {Promise<Response>}
     * @memberof Sysadmins
     */
    public async findAllEntries(internal?: boolean): Promise<Response | any> {
        const companies = await Company.aggregate([
            {
                $project: { name: 1, email: 1, avatar: 1, sidenote: 1, _id: 1 },
            },
        ]);

        return internal ? companies : this.response.status(200).json(companies);
    }

    /**
     * Create a new Sysadmin
     *
     * @returns {Promise<Response>}
     * @memberof Sysadmins
     */
    public async addNewEntry(internal?: boolean): Promise<Response | any> {
        const company: Document[] = <Document[]>await this.findOneEntry(true);

        if (company.length) {
            return internal ? { error: 'exists', company: company[0] } : this.response.status(500).json({ error: 'company-exists' });
        }

        const data = {
            name: this.body['companyname'],
            email: this.body['companyemail'],
            address: this.body['companyaddress'] ? this.body['companyaddress'] : null,
            location: this.body['location'] ? this.body['location'] : null,
            sidenote: this.body['sidenote'] ? this.body['sidenote'] : null,
            avatar: !this.request.files ? { file: null, path: null } : { file: this.request.files[0]['filename'], path: this.request.files[0]['path'] },
        };

        const account = new Company({
            _id: new Types.ObjectId(),
            ...data,
        });

        const created = await account.save();

        if (created['email'] == data.email) {
            return this.response.status(200).json({ message: 'success' });
        }

        return internal ? { message: 'okay', company: created['_id'] } : this.response.status(200).json({ message: 'failed' });
    }

    /**
     * Find a Sysadmin by their email address
     *
     * @param {boolean} [internal]
     * @returns {(Promise<Document[] | Response>)}
     * @memberof Sysadmins
     */
    public async findOneEntry(internal?: boolean): Promise<Document[] | Response> {
        const criteria: string = internal ? this.body['email'] : this.request.params['email'];

        const match: Document[] = await Company.aggregate([{ $match: { email: criteria } }]);
        return internal ? match : this.response.status(200).json(match);
    }
}
