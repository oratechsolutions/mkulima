// controller for Officer officers
import { hashSync } from 'bcrypt';
import { randomBytes } from 'crypto';
import { Request, Response } from 'express';
import { Document, Types } from 'mongoose';
import { Controller } from '../abstracts/Controller';
import { Email } from '../libraries/Email';
import { extractCookie } from '../middlewares/Cookies';
import Farmer from '../models/Farmer';
import Emailcreator from '../utilities/Emailcreator';

/**
 * System Farmer controller. Manage interaction with farmer model, create, update, retrieve and remove system.
 *
 * @export
 * @class Officers
 * @extends {Controller}
 */
export class Farmers extends Controller {
    public constructor(req: Request, res: Response) {
        super(req, res);
    }

    public async findOneAndUpdate(): Promise<Response> {
        const cookie = extractCookie(this.request.headers.cookie, 'mkl-f-ssid');
        const data = {
            name: this.body['firstname'] + ' ' + this.body['secondname'],
            email: this.body['email'],
            gender: this.body['gender'],
            phone: '+254 ' + this.body['phone'].slice(-9),
            location: this.body['location'],
            expertise: this.body['expertise'],
        };

        if (!this.body['avatar']) {
            data['avatar'] = {
                path: this.request.files[0]['filename'],
            };
        }
        if (this.body['password']) {
            data['password'] = hashSync(this.body['password'], 10);
        }

        const updated = await Farmer.findOneAndUpdate(
            {
                _id: Types.ObjectId(cookie.toString()),
            },
            {
                $set: { ...data },
            },
            {
                new: true,
            },
        );

        return this.response.status(200).json({ message: 'updated' });
    }

    /**
     * Find and return all Officers data, including profile information
     *
     * @returns {Promise<Response>}
     * @memberof Officers
     */
    public async findAllEntries(internal?: boolean): Promise<Response | any> {
        const officer: string = extractCookie(this.request.headers.cookie, 'mkl-xt-ssid') as string;
        const sysadmin: string = extractCookie(this.request.headers.cookie, 'mkl-sys-ssid') as string;

        const determineAdmin = function() {
            return officer ? { $match: { officer: Types.ObjectId(officer) } } : sysadmin ? { $match: {} } : null;
        };

        const farmers = await Farmer.aggregate([
            determineAdmin(),
            {
                $project: { name: 1, idnumber: 1, address: 1, email: 1, avatar: 1, officer: 1, phone: 1, createdat: 1, _id: 1 },
            },
            {
                $lookup: {
                    from: 'officers',
                    localField: 'officer',
                    foreignField: '_id',
                    as: 'extensionofficer',
                },
            },
            {
                $group: {
                    _id: { officer: '$officer', name: '$extensionofficer.name' },
                    farmers: {
                        $push: {
                            name: '$name',
                            idnumber: '$idnumber',
                            address: '$address',
                            phone: '$phone',
                            created: '$createdat',
                            avatar: '$avatar',
                            email: '$email',
                            id: '$_id',
                        },
                    },
                },
            },
        ]);

        return internal ? farmers : this.response.status(200).json(farmers);
    }

    /**
     * Create a new Farmer
     *
     * @returns {Promise<Response>}
     * @memberof Officers
     */
    public async addNewEntry(): Promise<Response> {
        const farmer: Document[] = await this.findOneEntry(true) as Document[];
        const password = randomBytes(3)
            .toString('hex')
            .toUpperCase();

        if (farmer.length) {
            return this.response.status(200).json({ error: 'farmer-exists' });
        }

        const data = {
            vcode: randomBytes(6)
                .toString('hex')
                .toUpperCase(),
            name: this.body['firstname'] + ' ' + this.body['secondname'],
            idnumber: this.body['idnumber'],
            address: this.body['address'],
            phone: this.body['phone'] ? '+254 ' + this.body['phone'].slice(-9) : null,
            email: this.body['email'],
            gender: this.body['gender'],
            avatar: {
                path: typeof this.request.files !== 'undefined' ? this.request.files[0].path : null,
            },
            password: hashSync(password, 10),
            officer: extractCookie(this.request.headers.cookie, 'mkl-xt-ssid'),
        };

        const profile = new Farmer({
            _id: new Types.ObjectId(),
            ...data,
        });

        const vemail = await new Emailcreator({
            template: 'confirmation',
        }).compile({ email: data.email, code: data.vcode, password });

        const emailstatus = new Email({
            recipients: [data.email],
            message: vemail,
            subject: 'Verify your account',
            fromtext: 'Mkulima service platform.',
            tocustomname: data.name,
        })
            .send()
            .catch((e) => e);

        const created = await profile.save();

        if (created['email'] === data.email) {
            // prompt to check their mail inboxes for verification code / link
            return this.response.status(200).json({ message: 'success' });
        }

        return this.response.status(500).json({ message: 'failed' });
    }

    /**
     * Find a Farmer by their email address
     *
     * @param {boolean} [internal]
     * @returns {(Promise<Document[] | Response>)}
     * @memberof Officers
     */
    public async findOneEntry(internal?: boolean): Promise<Document[] | Response> {
        const criteria: string = internal ? this.body['email'] : this.request.params['email'];

        const match: Document[] = await Farmer.aggregate([
            {
                $match: {
                    $or: [{ email: criteria }, { phone: this.body['phone'] ? '+254 ' + this.body['phone'].slice(-9) : null }, { idnumber: this.body['idnumber'] }],
                },
            },
        ]);

        return internal ? match : this.response.status(200).json(match);
    }
}
