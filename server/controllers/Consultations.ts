// controller for Officer Consultations
import { Request, Response } from 'express';
import { Document, Types } from 'mongoose';
import { Controller } from '../abstracts/Controller';
import { extractCookie } from '../middlewares/Cookies';
import Consultation from '../models/Consultation';
import Farmer from '../models/Farmer';

/**
 * System Consultation controller. Manage interaction with Consultation model, create, update, retrieve and remove system.
 *
 * @export
 * @class Consultations
 * @extends {Controller}
 */
export class Consultations extends Controller {
    public constructor(req: Request, res: Response) {
        super(req, res);
    }

    public async findOneAndUpdate(): Promise<Response> {
        throw new Error('Method not implemented.');
    }

    /**
     * Find and return all Consultations data, including profile information
     *
     * @returns {Promise<Response>}
     * @memberof Consultations
     */
    public async findAllEntries(): Promise<Response> {
        // expect sender and reciver ids
        const chat = {
            sender: extractCookie(this.request.headers.cookie, 'mkl-f-ssid') || extractCookie(this.request.headers.cookie, 'mkl-xt-ssid'),
            receiver: this.request.params['receiver'],
        };

        const criteria = this.request.params['receiver']
            ? {
                  $or: [
                      {
                          $and: [{ sender: Types.ObjectId(chat.sender.toString()) }, { receiver: Types.ObjectId(chat.receiver.toString()) }],
                      },
                      {
                          $and: [{ receiver: Types.ObjectId(chat.sender.toString()) }, { sender: Types.ObjectId(chat.receiver.toString()) }],
                      },
                  ],
              }
            : {
                  $and: [{ receiver: Types.ObjectId(chat.sender.toString()) }, { viewed: false }],
              };

        const consultations = await Consultation.aggregate([
            {
                $match: { ...criteria },
            },
            {
                $project: { sender: 1, receiver: 1, message: 1, attachments: 1, createdat: 1 },
            },
            {
                $sort: { createdat: 1 },
            },
        ]);

        return this.response.status(200).json(consultations);
    }

    /**
     * Create a new Consultation
     *
     * @returns {Promise<Response>}
     * @memberof Consultations
     */
    public async addNewEntry(): Promise<Response> {
        const data = {
            sender: extractCookie(this.request.headers.cookie, 'mkl-f-ssid') || extractCookie(this.request.headers.cookie, 'mkl-xt-ssid'),
            receiver: this.body['receiver'],
            message: this.body['message'],
            attachments: this.request.files ? this.request.files : null,
        };

        const consultation = new Consultation({
            _id: new Types.ObjectId(),
            ...data,
        });

        const created = await consultation.save();

        if (created['receiver'] === data.receiver) {
            return this.response.status(200).json({ message: 'success' });
        }

        return this.response.status(200).json({ message: 'failed' });
    }

    /**
     * Find a Consultation by their email address
     *
     * @param {boolean} [internal]
     * @returns {(Promise<Document[] | Response>)}
     * @memberof Consultations
     */
    public async findOneEntry(internal?: boolean): Promise<Document[] | Response> {
        const criteria: string = internal ? this.body['email'] : this.request.params['email'];

        const match: Document[] = await Consultation.aggregate([{ $match: { email: criteria } }]);
        return internal ? match : this.response.status(200).json(match);
    }

    public async groupUserConsultations(): Promise<Document[] | Response> {
        const chat = {
            sender: extractCookie(this.request.headers.cookie, 'mkl-xt-ssid'),
        };

        const criteria = {
            receiver: Types.ObjectId(chat.sender.toString()),
        };

        const match = await Farmer.aggregate([
            {
                $match: { officer: criteria.receiver },
            },
            {
                $lookup: {
                    from: 'consultations',
                    localField: '_id',
                    foreignField: 'sender',
                    as: 'chats',
                },
            },
            {
                $unwind: '$chats',
            },
            {
                $sort: { 'chats.createdat': 1 },
            },
            {
                $project: { email: 1, name: 1, phone: 1, idnumber: 1 },
            },
            {
                $group: {
                    _id: { email: '$email', name: '$name', phone: '$phone', idnumber: '$idnumber' },
                },
            },
        ]);

        console.log(match);

        return this.response.status(200).json(match);
    }
}
