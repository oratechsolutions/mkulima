import { Companies } from './Companies';
// controller for Officer Companies
import { hashSync, compareSync } from 'bcrypt';
import { randomBytes } from 'crypto';
import { Request, Response } from 'express';
import { Types, Document } from 'mongoose';
import { Controller } from '../abstracts/Controller';
import { Email } from '../libraries/Email';
import Superadmin from '../models/Superadmin';
import Emailcreator from '../utilities/Emailcreator';

/**
 * System Company controller. Manage interaction with Company model, create, update, retrieve and remove system administrators.
 *
 * @export
 * @class Companies
 * @extends {Controller}
 */
export class Superadmins extends Controller {
    public constructor(req: Request, res: Response) {
        super(req, res);
    }

    public async findOneAndUpdate(): Promise<Response> {
        throw new Error('Method not implemented.');
    }

    /**
     * Find and return all Companies data, including profile information
     *
     * @returns {Promise<Response>}
     * @memberof Companies
     */
    public async findAllEntries(): Promise<Response> {
        const superadmins = await Superadmin.aggregate([
            {
                $project: { name: 1, email: 1, avatar: 1 },
            },
        ]);

        return this.response.status(200).json(superadmins);
    }

    /**
     * Create a new Company
     *
     * @returns {Promise<Response>}
     * @memberof Companies
     */
    public async addNewEntry(): Promise<Response> {
        const superadmin: Document[] = await this.findOneEntry(true) as Document[];

        if (superadmin.length) {
            return this.response.status(200).json({ error: 'super-admin-exists' });
        }

        const company = new Companies(this.request, this.response).addNewEntry(true);
        console.log(company);
        const password = randomBytes(3)
            .toString('hex')
            .toUpperCase();

        const data = {
            vcode: randomBytes(6)
                .toString('hex')
                .toUpperCase(),
            name: this.body['firstname'] + ' ' + this.body['secondname'],
            email: this.body['email'],
            phone: '+254 ' + this.body['phone'].splice(-9),
            idnumber: this.body['idnumber'],
            password: hashSync(password, 10),
            company: this.body['company'],
        };

        const sadmin = new Superadmin({ _id: new Types.ObjectId(), ...data });

        const vemail = await new Emailcreator({ template: 'confirmation' }).compile({
            email: data.email,
            code: data.vcode,
            password,
        });

        const emailstatus = await new Email({
            recipients: [data.email],
            message: vemail,
            subject: 'Verify your account',
            fromtext: 'Mkulima service platform.',
            tocustomname: data.name,
        }).send();

        const created = await sadmin.save();

        if (created['email'] === data.email) {
            // prompt to check their mail inboxes for verification code / link
            return this.response.status(200).json({ message: 'success' });
        }

        return this.response.status(200).json({ message: 'failed' });
    }

    /**
     * Find a Company by their email address
     *
     * @param {boolean} [internal]
     * @returns {(Promise<Document[] | Response>)}
     * @memberof Companies
     */
    public async findOneEntry(internal?: boolean): Promise<Document[] | Response> {
        const criteria: string = internal ? this.body['email'] : this.request.params['email'];

        const match: Document[] = await Superadmin.aggregate([{ $match: { email: criteria } }]);
        return internal ? match : this.response.status(200).json(match);
    }
}
