import { Request, Response } from 'express';
import { AbstractRouter } from '../abstracts/Router';
import { Companies } from '../controllers/Companies';
import { Consultations } from '../controllers/Consultations';
import { Farmers } from '../controllers/Farmers';
import { Officers } from '../controllers/Officers';
import { secureConsultation, secureExtOfficer, secureFarmer, secureSuperAdmin, secureSysAdmin } from '../middlewares/Authentication';
import { uploader } from '../middlewares/Uploader';
import Consultation from '../models/Consultation';
import { Superadmins } from './../controllers/Superadmins';

/**
 *  Handle all requests
 *
 * @class ViewRouter
 */
class ViewRouter extends AbstractRouter {
    public constructor() {
        super();
        this.routeHandler();
        this.rejectHandler();
    }

    /**
     *  Listen for incoming requests
     *
     * @private
     * @memberof ViewRouter
     */
    protected routeHandler() {
        this.router.post('/company-data', secureSysAdmin, uploader.any(), async (req, res) => {
            if (!req.body['company'] || !req.body['companyemail']) {
                return res.json({ error: 'no-company-data' });
            }

            let company = '';
            if (req.body['companyemail']) {
                company = await new Companies(req, res).addNewEntry(true);
                req.body['company'] = company;
            } else {
                company = req.body['company'];
            }

            return console.log(req.body);
            // return await new Sysadmins(req, res).addNewEntry();
        });

        this.router.post('/company-admin', secureSysAdmin, (req, res) => {
            new Superadmins(req, res).addNewEntry();
        });
        this.router.get('/company-admins', secureSysAdmin, (req, res) => {
            new Superadmins(req, res).findAllEntries();
        });

        this.router.post('/officers', secureSuperAdmin, (req, res) => {
            new Officers(req, res).addNewEntry();
        });
        this.router.post('/officer/update', secureExtOfficer, uploader.any(), (req, res) => {
            new Officers(req, res).findOneAndUpdate();
        });
        this.router.get(
            '/officers',
            /* secureSuperAdmin ,*/ (req, res) => {
                new Officers(req, res).findAllEntries();
            }
        );
        this.router.get('/officer', (req, res) => {
            new Officers(req, res).findOneEntry();
        });

        this.router.post('/farmers', secureExtOfficer, uploader.any(), (req, res) => {
            new Farmers(req, res).addNewEntry();
        });
        this.router.get('/farmers', (req, res) => {
            new Farmers(req, res).findAllEntries();
        });
        this.router.post('/farmer/update', secureFarmer, uploader.any(), (req, res) => {
            new Farmers(req, res).findOneAndUpdate();
        });

        this.router.get('/consultation/:receiver', secureConsultation, (req, res) => {
            new Consultations(req, res).findAllEntries().catch(e => e);
        });
        this.router.get('/consultations', secureConsultation, (req, res) => {
            new Consultations(req, res).findAllEntries().catch(e => e);
        });
        this.router.get('/grouped-consultations', secureConsultation, (req, res) => {
            new Consultations(req, res).groupUserConsultations().catch(e => console.log(e));
        });
        this.router.post('/consultation', secureConsultation, (req, res) => {
            new Consultations(req, res).addNewEntry();
        });

        this.router.post('/mark-read-consultations', (req, res) => {
            Consultation.updateMany({ _id: { $in: req.body['chatIds'] } }, { $set: { viewed: true } }).exec();
            res.end();
        });
    }

    /**
     *  Reject unhandled routes
     *
     * @private
     * @memberof ViewRouter
     */
    protected rejectHandler() {
        this.router.get('*', (req: Request, res: Response) => {
            res.status(404).json({ error: 'ENOENT' });
        });

        this.router.post('*', (req: Request, res: Response) => {
            res.status(404).json({ error: 'unhandled post route' });
        });
    }
}

module.exports = new ViewRouter().route();
