import { Companies } from './../controllers/Companies';
import { Types } from 'mongoose';
import { Officers } from './../controllers/Officers';
import { Farmers } from './../controllers/Farmers';
import { Request, Response } from 'express';
import { AbstractRouter } from '../abstracts/Router';
import Farmer from '../models/Farmer';
import { extractCookie } from '../middlewares/Cookies';
import Officer from '../models/Officer';
import Sysadmin from '../models/Sysadmin';
import Superadmin from '../models/Superadmin';
import Consultation from '../models/Consultation';
import { secureSysAdmin } from '../middlewares/Authentication';

/**
 *  Handle all requests
 *
 * @class RouterInstance
 */
class RouterInstance extends AbstractRouter {
    /**
     * Creates an instance of RouterInstance.
     * @memberof RouterInstance
     */
    public constructor() {
        super();
        this.routeHandler();
        this.rejectHandler();
    }

    /**
     *  Listen for incoming requests
     *
     * @private
     * @memberof RouterInstance
     */
    protected routeHandler() {
        async function loggedInUser(req: Request, user: string, cookie: string) {
            const model = {
                farmer: Farmer,
                officer: Officer,
                sysadmin: Sysadmin,
                superadmin: Superadmin,
            };

            const id = extractCookie(req.headers.cookie, cookie);

            if (!id) {
                return { name: 'Not logged in!' };
            }

            const match = await model[user].aggregate([{ $match: { _id: Types.ObjectId(id.toString()) } }]);

            return {
                name: match[0]['name'] ? match[0]['name'] : 'Not logged in',
                avatar: match[0]['avatar'] ? match[0]['avatar']['path'] : null,
            };
        }


        this.router.use('/farmer', (req, res, next) => {
            const cookie = extractCookie(req.headers.cookie, 'mkl-f-ssid');
            !cookie ? res.redirect('/') : next();
        });

        this.router.use('/officer', (req, res, next) => {
            const cookie = extractCookie(req.headers.cookie, 'mkl-xt-ssid');
            !cookie ? res.redirect('/') : next();
        });

        // rendering general pages
        this.router.get('/', (req, res) => {
            const message = req.query['m'] === 'non-existent' ? 'Email address provided does not exist!' : req.query['m'] === 'password-mismatch' ? 'Password mismatch!' : '';
            res.render('auth/index', { message });
        });

        // rendering farmer dashboard pages
        this.router.get('/farmer', async (req, res) => {
            res.render('farmer/dashboard', await loggedInUser(req, 'farmer', 'mkl-f-ssid'));
        });
        this.router.get('/farmer/chat', async (req, res) => {
            const admin = await Farmer.aggregate([
                {
                    $match: {
                        _id: Types.ObjectId(extractCookie(req.headers.cookie, 'mkl-f-ssid').toString()),
                    },
                },
                {
                    $lookup: {
                        from: 'officers',
                        localField: 'officer',
                        foreignField: '_id',
                        as: 'admin',
                    },
                },
            ]);

            res.render('farmer/chat', {
                ...(await loggedInUser(req, 'farmer', 'mkl-f-ssid')),
                admin: admin[0]['admin'] ? admin[0]['admin'][0] : [],
            });
        });

        this.router.get('/farmer/profile', async (req, res) => {
            const cookie = extractCookie(req.headers.cookie, 'mkl-f-ssid');

            const farmer = await Farmer.aggregate([
                {
                    $match: { _id: Types.ObjectId(cookie.toString()) },
                },
                {
                    $lookup: {
                        from: 'officers',
                        localField: 'officer',
                        foreignField: '_id',
                        as: 'officer',
                    },
                },
                {
                    $lookup: {
                        from: 'consultations',
                        localField: '_id',
                        foreignField: 'sender',
                        as: 'replies',
                    },
                },
                {
                    $lookup: {
                        from: 'consultations',
                        localField: '_id',
                        foreignField: 'receiver',
                        as: 'received',
                    },
                },
            ]);

            res.render('farmer/user_profile', {
                ...(await loggedInUser(req, 'farmer', 'mkl-f-ssid')),
                farmer: farmer[0],
            });
        });
        this.router.get('/farmer/editprofile', async (req, res) => {
            res.render('farmer/edit_profile', await loggedInUser(req, 'farmer', 'mkl-f-ssid'));
        });
        this.router.get('/farmer/consultations', async (req, res) => {
            res.render('farmer/consultations', await loggedInUser(req, 'farmer', 'mkl-f-ssid'));
        });

        // rendering officer dashboard pages
        this.router.get('/officer', async (req, res) => {
            const cookie = extractCookie(req.headers.cookie, 'mkl-xt-ssid');

            const consults = await Consultation.aggregate([
                {
                    $match: { $or: [{ receiver: Types.ObjectId(cookie.toString()) }] },
                },
            ]).count('consults');

            const replies = await Consultation.aggregate([
                {
                    $match: { $or: [{ sender: Types.ObjectId(cookie.toString()) }] },
                },
            ]).count('replies');

            const users = await Farmer.aggregate([
                {
                    $match: { officer: Types.ObjectId(cookie.toString()) },
                },
            ]).count('farmers');

            res.render('officer/dashboard', { ...(await loggedInUser(req, 'officer', 'mkl-xt-ssid')), consults, replies, users});
        });
        this.router.get('/officer/add_farmer', async (req, res) => {
            res.render('officer/add_farmer', await loggedInUser(req, 'officer', 'mkl-xt-ssid'));
        });
        this.router.get('/officer/all_farmers', async   (req, res) => {
            let farmers = await new Farmers(req, res).findAllEntries(true);
            farmers = farmers.length ? farmers[0].farmers : [];
            res.render('officer/all_farmers', {
                farmers,
                ...(await loggedInUser(req, 'officer', 'mkl-xt-ssid')),
            });
        });
        this.router.get('/officer/adminstrators', async (req, res) => {
            const officers = await new Officers(req, res).findAllEntries(true);
            res.render('officer/adminstrators', {
                officers,
                ...(await loggedInUser(req, 'officer', 'mkl-xt-ssid')),
            });
        });
        this.router.get('/officer/consultations', async (req, res) => {
            let farmers = await new Farmers(req, res).findAllEntries(true);
            farmers = farmers.length ? farmers[0].farmers : [];
            res.render('officer/consultations', {
                farmers,
                ...(await loggedInUser(req, 'officer', 'mkl-xt-ssid')),
            });
        });
        this.router.get('/officer/chats', async (req, res) => {
            let farmers = await new Farmers(req, res).findAllEntries(true);
            farmers = farmers.length ? farmers[0].farmers : [];
            res.render('officer/chats', {
                farmers,
                ...(await loggedInUser(req, 'officer', 'mkl-xt-ssid')),
            });
        });
        this.router.get('/officer/user_profile', async (req, res) => {
            const cookie = extractCookie(req.headers.cookie, 'mkl-xt-ssid');

            const officer = await Officer.aggregate([
                {
                    $match: { _id: Types.ObjectId(cookie.toString()) },
                },
                {
                    $lookup: {
                        from: 'superadmins',
                        localField: 'admin',
                        foreignField: '_id',
                        as: 'superadmin',
                    },
                },
                {
                    $lookup: {
                        from: 'consultations',
                        localField: '_id',
                        foreignField: 'sender',
                        as: 'replies',
                    },
                },
                {
                    $lookup: {
                        from: 'consultations',
                        localField: '_id',
                        foreignField: 'receiver',
                        as: 'received',
                    },
                },
            ]);

            res.render('officer/user_profile', {
                ...(await loggedInUser(req, 'officer', 'mkl-xt-ssid')),
                officer: officer[0],
            });
        });
        this.router.get('/officer/edit_profile', async (req, res) => {
            res.render('officer/edit_profile', await loggedInUser(req, 'officer', 'mkl-xt-ssid'));
        });

        // rendering superadmin dashboard pages
        this.router.get('/s-dashboard', (req, res) => {
            res.render('superadmin/su_dashboard');
        });
        this.router.get('/s-add-officer', (req, res) => {
            res.render('superadmin/su_admin_add');
        });
        this.router.get('/s-all-officers', (req, res) => {
            res.render('superadmin/su_officers');
        });
        this.router.get('/s-profile', (req, res) => {
            res.render('superadmin/su_profile');
        });
        this.router.get('/s-profile-edit', (req, res) => {
            res.render('superadmin/su_profile_edit');
        });
        this.router.get('/s-farmers', (req, res) => {
            res.render('superadmin/su_farmers_view');
        });

        // render system admin views
        this.router.get('/sys-dashboard', secureSysAdmin, (req, res) => {
            res.render('sysadmin/sys-dashboard');
        });
        this.router.get('/companies', secureSysAdmin, (req, res) => {
            res.render('sysadmin/companies');
        });
        this.router.get('/register-company', secureSysAdmin, async (req, res) => {
            const companies = await new Companies(req, res).findAllEntries(true);
            res.render('sysadmin/register-company', { companies });
        });
        this.router.get('/regis', secureSysAdmin, (req, res) => {
            res.render('sysadmin/session-pending');
        });

        // clear cookies related to this platform
        this.router.get('/logout', async (req, res) => {
            const cookies: {} = extractCookie(req.headers.cookie) as {};
            for (const cookie in cookies) {
                if (cookie.includes('mkl')) { res.clearCookie(cookie); }
            }
            res.redirect('/');
        });
    }

    /**
     *  Reject unhandled routes
     *
     * @private
     * @memberof RouterInstance
     */
    protected rejectHandler() {
        this.router.get('*', (req: Request, res: Response) => {
            res.end();
        });
        this.router.post('*', (req: Request, res: Response) => {
            res.status(404).json({ error: 'undefine-post-route' });
        });
    }
}

module.exports = new RouterInstance().route();
