import { Request, Response } from 'express';
import { AbstractRouter } from '../abstracts/Router';
import { Farmers } from '../controllers/Farmers';
import { Sysadmins } from '../controllers/Sysadmins';
import { extractCookie } from '../middlewares/Cookies';
import { Officers } from '../controllers/Officers';
import { Superadmins } from './../controllers/Superadmins';
import { Document } from 'mongoose';
import { compareSync } from 'bcrypt';

/**
 * Data placeholders for returned matches during sign in.
 *
 * @interface IAuth
 */
interface IAuth {
    user: string;
    cookie: string;
    path: string;
    value: string;
    password: string;
}

/**
 * Handles user authentication
 *
 * @class AuthRoutes
 * @extends {AbstractRouter}
 */
class AuthRoutes extends AbstractRouter {
    /**
     * Creates an instance of AuthRoutes.
     * @memberof AuthRoutes
     */
    constructor() {
        super();
        this.routeHandler();
        this.rejectHandler();
    }

    /**
     * Handles authentication requests and forwards to relevant controllers
     *
     * @protected
     * @memberof AuthRoutes
     */
    protected routeHandler(): void {
        /**
         * Handle authentication into the platform and set appropriate authentication cookies
         *
         * @param {Request} req - incoming request object
         * @param {Response} res - outgoing response object
         * @returns {Promise<IAuth>}
         */
        async function authenticateSignIns(req: Request, res: Response): Promise<IAuth> {
            const data: IAuth = { user: null, path: null, cookie: null, value: null, password: null };

            const assign = (options: { cookie: string; path: string; data: Document[] }) => {
                data['user'] = options.path;
                data['cookie'] = options.cookie;
                data['value'] = options.data[0]['_id'];
                data['path'] = options.path;
                data['password'] = options.data[0]['password'];
            };

            const sys: Document[] = (await new Sysadmins(req, res).findOneEntry(true)) as Document[];
            if (sys.length) {
                assign({ cookie: 'mkl-sys-ssid', path: '/sys-dashboard', data: sys });
            }
            const adm: Document[] = (await new Superadmins(req, res).findOneEntry(true)) as Document[];
            if (adm.length) {
                assign({ cookie: 'mkl-a-ssid', path: '/admin', data: adm });
            }
            const officer: Document[] = (await new Officers(req, res).findOneEntry(true)) as Document[];
            if (officer.length) {
                assign({ cookie: 'mkl-xt-ssid', path: '/officer', data: officer });
            }
            const farmer: Document[] = (await new Farmers(req, res).findOneEntry(true)) as Document[];
            if (farmer.length) {
                assign({ cookie: 'mkl-f-ssid', path: '/farmer', data: farmer });
            }

            return data;
        }

        this.router.post('/sys/signup', (req, res) => {
            new Sysadmins(req, res).addNewEntry();
        });

        this.router.post('/signin', async (req, res) => {
            const match = await authenticateSignIns(req, res);

            if (!match.user) {
                return res.redirect('/?m=non-existent');
            }

            if (!compareSync(req.body['password'], match.password)) {
                return res.redirect('/?m=password-mismatch');
            }

            // terminate all other existing sessions before allowing incoming login request into the system
            // prevent access to data that would otherwise leak to unauthorized sessions
            const cookies: {} = extractCookie(req.headers.cookie) as {};
            for (const cookie in cookies) {
                if (cookie.includes('mkl')) {
                    res.clearCookie(cookie);
                }
            }

            res.cookie(match.cookie, match.value.toString(), { maxAge: 1000 * 60 * 60 * 24 });
            // { message: 'authenticated' }
            return res.redirect(match.path);
        });
    }

    /**
     * Unhandled route requests to authentication will be rejected automatically
     *
     * @protected
     * @memberof AuthRoutes
     */
    protected rejectHandler(): void {
        this.router.post('*', (req, res) => {
            res.status(500).json({ error: 'Internal server error' });
        });
        this.router.get('*', (req, res) => {
            res.status(404).json({ error: 'ENOENT' });
        });
    }
}

module.exports = new AuthRoutes().route();
