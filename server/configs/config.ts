require('dotenv').config();

export const MONGO_LOCAL_URL = `mongodb://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_HOST}:27017/${process.env.MONGO_DATABASE}`;

export const MONGO_PROD_URL = 'mongodb+srv://mkulima:25812345Dan@mkulima-cluster-rrdcp.gcp.mongodb.net/test?retryWrites=true';

export const APP_EMAIL_ADDRESS = process.env.APP_EMAIL_ADDRESS;

export const APP_EMAIL_PASSWORD = process.env.APP_EMAIL_PASSWORD;

export const APP_EMAIL_HOST = process.env.APP_EMAIL_HOST;

export const [SMS_USERNAME, SMS_API_KEY] = ['', ''];
