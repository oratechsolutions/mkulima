import { Schema, model } from 'mongoose';

const Consultation = new Schema({
    _id: {
        type: Schema.Types.ObjectId,
        required: true,
    },
    sender: {
        type: Schema.Types.ObjectId,
        required: true,
    },
    receiver: {
        type: Schema.Types.ObjectId,
        required: true,
    },
    message: {
        type: String,
        required: true,
    },
    attachments: [
        {
            path: {
                type: String,
            },
            mime: {
                type: String,
                enum: ['docx', 'xlsx', 'csv', 'pdf', 'img', 'video'],
            },
        },
    ],
    viewed: {
        type: Boolean,
        default: false,
    },
    createdat: {
        type: Date,
        default: Date.now,
    },
    updatedat: {
        type: Date,
        default: Date.now,
    },
});

export default model('Consultations', Consultation);
