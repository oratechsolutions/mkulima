import { Schema, model } from 'mongoose';

const company = new Schema({
    _id: {
        type: Schema.Types.ObjectId,
        required: true,
    },
    name: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
    address: {
        type: String,
    }, 
    location: {
        type: String,
    },
    avatar: {
        file: {
            type: String,
        },
        path: {
            type: String,
        },
    },
    sidenote: {
        type: String,
    },
});

export default model('Companies', company);
