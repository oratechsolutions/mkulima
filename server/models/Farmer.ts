import { model, Schema, HookNextFunction } from 'mongoose';

const farmer = new Schema({
    _id: {
        type: Schema.Types.ObjectId,
        required: true,
    },
    name: {
        type: String,
        required: true,
    },
    idnumber: {
        type: Number,
        required: true,
    },
    address: {
        type: String,
    },
    email: {
        type: String,
    },
    phone: {
        type: String,
        required: true,
    },
    password: {
        type: String,
    },
    officer: {
        type: Schema.Types.ObjectId,
        required: true,
    },
    gender: {
        type: String,
    },
    location: {
        type: String,
    },
    avatar: {
        path: {
            type: String,
        },
    },
    expertise: {
        type: String,
    },
    vcode: {
        type: String,
        required: true,
    },
    verified: {
        type: Boolean,
        default: false,
    },
    createdat: {
        type: Date,
        default: Date.now,
    },
    updatedat: {
        type: Date,
        default: Date.now,
    },
});

export default model('Farmers', farmer);

farmer.pre('update', function(next: HookNextFunction) {
    this['updatedat'] = Date.now;
    next();
});
