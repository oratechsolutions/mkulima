import { Schema, model } from 'mongoose';

const sysadmin = new Schema({
    _id: {
        type: Schema.Types.ObjectId,
        required: true,
    },
    name: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    vcode: {
        type: String,
        required: true,
    },
    company: {
        type: Schema.Types.ObjectId,
        required: true,
    },
    vstatus: {
        type: Boolean,
        default: false,
    },
    createdat: {
        type: Date,
        default: Date.now,
    },
});

export default model('Sysadmin', sysadmin);
