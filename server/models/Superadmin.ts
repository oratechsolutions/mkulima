import { Schema, model } from 'mongoose';

const superadmin = new Schema({
    _id: {
        type: Schema.Types.ObjectId,
        required: true,
    },
    name: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
    idnumber: {
        type: String,
        required: true,
    },
    phone: {
        type: String,
        required: true,
    },
    vcode: {
        type: String,
        required: true,
    },
    avatar: {
        file: {
            type: String,
        },
        path: {
            type: String,
        },
    },
    company: {
        type: Schema.Types.ObjectId,
        required: true,
    },
});

export default model('Superadmins', superadmin);
