import { Schema, model } from 'mongoose';

const officer = new Schema({
    _id: {
        type: Schema.Types.ObjectId,
        required: true,
    },
    name: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    avatar: {
        path: {
            type: String,
        },
    },
    phone: {
        type: String,
        default: '+254 700 ...',
    },
    admin: {
        type: Schema.Types.ObjectId,
        required: true,
    },
    gender: {
        type: String,
    },
    expertise: {
        type: String,
    },
    idnumber: {
        type: Number,
    },
    location: {
        type: String,
    },
});

export default model('Officers', officer);
