import { Request, Response, NextFunction } from 'express';
import { extractCookie } from './Cookies';

/**
 * Secure system access to only authenticated requests, for system administrator
 *
 * Use cookie authentication or headers if cookies are missing
 * during sign in, cookies sent to the client are expected to be sent to the server as a payload on all subsequent requests
 *
 * A fallback to header authentication will be used when no cookies are found in a request,
 * Missing cookies and authentication headers will cause a rejection to the incoming request
 *
 * @export
 * @param {Request} req incoming request object
 * @param {Response} res outgoing response object
 * @param {NextFunction} next application level next function, to pass execution when expected criteria is met
 * @returns {(Promise<void | Response>)}
 */
export async function secureSysAdmin(req: Request, res: Response, next: NextFunction): Promise<void | Response> {
    let authvalue: string | object = extractCookie(req.headers.cookie, 'mkl-sys-ssid');
    if (!authvalue) {
        // incoming authentication header value is expected to be in the form
        // {'mkl-sys-ssid': 'Authenticate header-value-as-read-from-cookies'}

        // header 'mkl-sys-ssid' might be missing therefore handle this error before it occurs
        try {
            authvalue = req.headers['mkl-sys-ssid'].toString().split(' ')[1];
            // tslint:disable-next-line:no-empty
        } catch {}
    }
    if (authvalue) {
        return next();
    }
    return res.status(304).redirect('/');
}

/**
 * Secure path and data access that requires admins to be authenticated.
 *
 * Use cookie authentication or headers if cookies are missing
 * during sign in, cookies sent to the client are expected to be sent as a payload on all subsequent requests.
 *
 *
 * A fallback to header authentication will be used when no cookies are found in a request,
 * missing cookies and authentication headers will cause a rejection to the incoming request
 *
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {(Promise<void | Response>)}
 */
export async function secureSuperAdmin(req: Request, res: Response, next: NextFunction): Promise<void | Response> {
    let authvalue = extractCookie(req.headers.cookie, 'mkl-a-ssid');
    if (!authvalue) {
        // incoming authentication header value is expected to be in the form
        // {'mkl-a-ssid': 'Authenticate header-value-as-read-from-cookies'}

        // header 'mkl-a-ssid' might be missing therefore handle this error before it occurs
        try {
            authvalue = req.headers['mkl-a-ssid'].toString().split(' ')[1];
            // tslint:disable-next-line:no-empty
        } catch {}
    }
    if (authvalue) {
        return next();
    }
    return res.status(403).json({ error: 'forbidden' });
}

/**
 * Secure access to data and path that requires extension officers to be authenticated.
 *
 * Use cookie authentication or headers if cookies are missing
 * during sign in, cookies sent to the client are expected to be sent as a payload on all subsequent requests.
 *
 *
 * A fallback to header authentication will be used when no cookies are found in a request,
 * missing cookies and authentication headers will cause a rejection to the incoming request
 *
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {(Promise<void | Response>)}
 */
export async function secureExtOfficer(req: Request, res: Response, next: NextFunction): Promise<void | Response> {
    let authvalue = extractCookie(req.headers.cookie, 'mkl-xt-ssid');

    if (!authvalue) {
        // incoming authentication header value is expected to be in the form
        // {'mkl-xt-ssid': 'Authenticate header-value-as-read-from-cookies'}

        // header 'mkl-xt-ssid' might be missing therefore handle this error before it occurs
        try {
            authvalue = req.headers['mkl-xt-ssid'].toString().split(' ')[1];
            // tslint:disable-next-line:no-empty
        } catch {}
    }
    if (authvalue) {
        return next()
    }
    return res.status(403).json({ error: 'forbidden' });
}

/**
 * Secure access to data and path that requires farmers to be authenticated.
 *
 * Use cookie authentication or headers if cookies are missing
 * during sign in, cookies sent to the client are expected to be sent as a payload on all subsequent requests.
 *
 *
 * A fallback to header authentication will be used when no cookies are found in a request,
 * missing cookies and authentication headers will cause a rejection to the incoming request
 *
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {(Promise<void | Response>)}
 */
export async function secureFarmer(req: Request, res: Response, next: NextFunction): Promise<void | Response> {
    let authvalue = extractCookie(req.headers.cookie, 'mkl-f-ssid');
    if (!authvalue) {
        // incoming authentication header value is expected to be in the form
        // {'mkl-f-ssid': 'Authenticate header-value-as-read-from-cookies'}

        // header 'mkl-f-ssid' might be missing therefore handle this error before it occurs
        try {
            authvalue = req.headers['mkl-f-ssid'].toString().split(' ')[1];
            // tslint:disable-next-line:no-empty
        } catch {}
    }
    if (authvalue) {
        return next();
    }
    return res.status(403).json({ error: 'forbidden' });
}

/**
 * Authenticate every consultancy communication. Requires extnsion officers and farmers to be authenticated.
 *
 * Consultancy can only be between farmers and extension officers. Failure to which messages will be rejected.
 *
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {(Promise<void | Response>)}
 */
export async function secureConsultation(req: Request, res: Response, next: NextFunction): Promise<void | Response> {
    let authvalue = extractCookie(req.headers.cookie, 'mkl-f-ssid') || extractCookie(req.headers.cookie, 'mkl-xt-ssid');
    if (!authvalue) {
        // incoming authentication header value is expected to be in the form
        // {'mkl-f-ssid': 'Authenticate header-value-as-read-from-cookies'}

        // header 'mkl-f-ssid' might be missing therefore handle this error before it occurs
        try {
            authvalue = req.headers['mkl-f-ssid'].toString().split(' ')[1] || req.headers['mkl-xt-ssid'].toString().split(' ')[1];
            // tslint:disable-next-line:no-empty
        } catch {}
    }
    if (authvalue) {
        return next();
    }
    return res.status(403).json({ error: 'forbidden' });
}
