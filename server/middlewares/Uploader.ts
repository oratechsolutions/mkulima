import * as multer from 'multer';
import { randomBytes } from 'crypto';
import { mkdirSync } from 'fs';
import * as path from 'path';

/**
 * Handle file uploads, including documents and images
 *
 * @export uploader
 */
export const uploader: multer.Instance = multer({
    storage: multer.diskStorage({
        destination: (req, file, cb) => {
            try {
                mkdirSync(path.join(__dirname, '..', '..', 'uploads'));
                // tslint:disable-next-line:no-empty
            } catch {}
            cb(null, path.join(__dirname, '..', '..', 'uploads'));
        },
        filename: (req, file, cb) => {
            const customFileName = randomBytes(18).toString('hex');
            const fileExtension = file.originalname.split('.')[1];
            cb(null, customFileName + '.' + fileExtension);
        },
    }),
});
